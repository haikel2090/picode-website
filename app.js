var newImg = 0;
var images = [
  "images/capture1.jpg", "images/capture.png", "images/capture2.png", "images/capture3.png",
  "images/capture4.png"
];

var imageEl = document.getElementById('img');

setInterval(() => {
  if (newImg > 4) { newImg = 0 }
  document.getElementById('img').src = images[newImg];
  newImg++;
}, 4000);